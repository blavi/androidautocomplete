package com.example.laviniabratis.androidautocomplete.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.laviniabratis.androidautocomplete.R;
import com.example.laviniabratis.androidautocomplete.helper.ResultsAdapter;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lavinia.bratis on 9/8/15.
 */
public class AutoCompleteWithListFragment extends Fragment {
    private Activity activity;
    private TextWatcher searchTextWatcher;
    private Timer timer;
    private int AUTOCOMPLETE_DELAY = 500;
    private EditText searchInput;
    private ListView resultsList;
    private ResultsAdapter mAdapter;
    private ProgressBar pbLoadingIndicator;
    private View view;

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);

        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.second_tab, container, false);

        setLayout();

        return view;
    }

    private void setLayout(){
        searchInput = (EditText) view.findViewById(R.id.searchInput);
        resultsList = (ListView) view.findViewById(R.id.resultsList);
        pbLoadingIndicator = (ProgressBar) view.findViewById(R.id.pbLoadingIndicator);

        setContent();

        setTextWatcher();
    }

    private void setTextWatcher(){
        final Filter filter = mAdapter.getFilter();
        searchTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // user typed: start the timer
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        filter.filter(searchInput.getText().toString());

                        // hide keyboard
                        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }, AUTOCOMPLETE_DELAY); // 500ms delay before the timer executes the „run“ method from TimerTask
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
            }
        };

        searchInput.addTextChangedListener(searchTextWatcher);
    }

    private void setContent(){
        mAdapter = new ResultsAdapter(activity);
        mAdapter.setLoadingIndicator(pbLoadingIndicator);
        resultsList.setAdapter(mAdapter);

        resultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                searchInput.setText(mAdapter.getItem(position));
            }
        });
    }

}
