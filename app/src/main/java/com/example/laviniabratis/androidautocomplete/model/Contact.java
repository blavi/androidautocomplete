package com.example.laviniabratis.androidautocomplete.model;

/**
 * Created by lavinia.bratis on 8/5/15.
 */
public class Contact{
    private String name;
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }


    public Contact(String name, String phoneNumber){
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        Contact contact = (Contact) o;
        return contact.getName().equals(this.getName()) && contact.getPhoneNumber().equals(((Contact) o).getPhoneNumber());
    }
}
