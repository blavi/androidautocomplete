package com.example.laviniabratis.androidautocomplete.helper;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavinia.bratis on 9/3/15.
 */
public class GetResults {

    private static GetResults instance;
    private Context context;

    private GetResults(Context context){
        this.context = context;
    }

    public static GetResults getInstance(Context context){
        if (instance == null) {
            instance = new GetResults(context);
        }
        return instance;
    }


    public List<String> loadFromWeb(String keyword){
        List<String> results = new ArrayList<>();

        try {
            Log.d("REQUEST", keyword);
            String location = String.format(Constants.URL, Constants.API_KEY, keyword, Constants.ENGINE_ID);
            URL url = new URL(location); // + keyword + api_key + engine_id

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;
            StringBuilder content = new StringBuilder();

            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();

            JsonElement jelement = new JsonParser().parse(content.toString());
            JsonObject jobject = (jelement != null) ? jelement.getAsJsonObject() : null;
            JsonArray jarray = (jobject != null) ? jobject.getAsJsonArray(Constants.JSON_ITEMS) : null;
            if (jarray != null) {
                for (int i = 0; i < jarray.size(); i++) {
                    jobject = jarray.get(i).getAsJsonObject();
                    String str = jobject.get(Constants.JSON_TITLE).toString();
                    String title = str.substring(1, str.length()-1);
                    results.add(title);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }
}
