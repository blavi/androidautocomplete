package com.example.laviniabratis.androidautocomplete.helper;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

/**
 * Created by lavinia.bratis on 9/4/15.
 */
public class DelayAutoCompleteTextView extends AutoCompleteTextView {

    private static final int MESSAGE_TEXT_ID = 1;
    private static final int AUTOCOMPLETE_DELAY = 500;
    private ProgressBar mLoadingIndicator;
    private Context mContext;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            DelayAutoCompleteTextView.super.performFiltering((CharSequence) msg.obj, msg.arg1);
        }
    };

    public DelayAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public void setLoadingIndicator(ProgressBar progressBar) {
        mLoadingIndicator = progressBar;
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        if (mLoadingIndicator != null) {
            mLoadingIndicator.setVisibility(View.VISIBLE);
        }

        mHandler.removeMessages(MESSAGE_TEXT_ID);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MESSAGE_TEXT_ID, text), AUTOCOMPLETE_DELAY);
    }

    @Override
    public void onFilterComplete(int count) {
        super.onFilterComplete(count);

        if (mLoadingIndicator != null) {
            mLoadingIndicator.setVisibility(View.GONE);
        }
        InputMethodManager in = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(this.getApplicationWindowToken(), 0);
    }
}
