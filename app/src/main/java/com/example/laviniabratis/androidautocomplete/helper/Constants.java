package com.example.laviniabratis.androidautocomplete.helper;

/**
 * Created by lavinia.bratis on 9/3/15.
 */
public class Constants {

    public static final String URL = "https://www.googleapis.com/customsearch/v1?key=%s&q=%s&cx=%s";
    public static final String API_KEY = "AIzaSyB551mCxIWZG3Dsr0H0LGkXaLdgEl-n-gw";
    public static final String ENGINE_ID = "000448205074692022280:b8zxhjeziqa";

    public static final String JSON_TITLE = "title";
    public static final String JSON_ITEMS = "items";
}
