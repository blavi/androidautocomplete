package com.example.laviniabratis.androidautocomplete.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavinia.bratis on 8/6/15.
 */
public class PhoneContacts {

    private List<Contact> phoneContacts;

    public PhoneContacts(Contact contact){
        this.phoneContacts = new ArrayList<>();
        phoneContacts.add(contact);
    }

    public PhoneContacts(List<Contact> contacts){
        this.phoneContacts = contacts;
    }

    public Contact getContact(int position){
        return phoneContacts.get(position);
    }

    public int getNumberOfContacts(){
        if (phoneContacts != null)
            return phoneContacts.size();
        return 0;
    }

    public PhoneContacts filterContactsByName(String name){

        ArrayList<Contact> filteredContacts = new ArrayList<>();

        for (Contact contact : phoneContacts){
            if (contact.getName().regionMatches(true, 0, name, 0, name.length())){
                filteredContacts.add(contact);
            }
        }

        return new PhoneContacts(filteredContacts);
    }

    public PhoneContacts filterContactsByPhoneNumber(String phoneNumber){

        ArrayList<Contact> filteredContacts = new ArrayList<>();

        for (Contact contact : phoneContacts){
            if (contact.getPhoneNumber().startsWith(phoneNumber)){
                filteredContacts.add(contact);
            }
        }

        return new PhoneContacts(filteredContacts);
    }

    public void clearContacts(){
        phoneContacts.clear();
    }

}
