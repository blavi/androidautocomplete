package com.example.laviniabratis.androidautocomplete.helper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.laviniabratis.androidautocomplete.R;
import com.example.laviniabratis.androidautocomplete.model.Contact;
import com.example.laviniabratis.androidautocomplete.model.PhoneContacts;

import java.util.regex.Pattern;

/**
 * Created by lavinia.bratis on 9/3/15.
 */
public class ContactsCursorAdapter extends BaseAdapter implements Filterable{

    private Context mContext;
    private PhoneContacts contacts;
    private PhoneContacts allPhoneContacts;
    private ProgressBar mLoadingIndicator;

    public ContactsCursorAdapter(Context context, PhoneContacts contacts) {
        mContext = context;
        this.allPhoneContacts = contacts;
        updatePhoneContacts(null);
    }

    @Override
    public int getCount() {
        return contacts != null ? contacts.getNumberOfContacts() : 0;
    }

    @Override
    public Contact getItem(int index) {
        return contacts != null ? contacts.getContact(index) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.contact_item, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.name)).setText(getItem(position).getName());
        ((TextView) convertView.findViewById(R.id.phone_number)).setText(getItem(position).getPhoneNumber());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (mLoadingIndicator != null) {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        public void run() {
                            mLoadingIndicator.setVisibility(View.VISIBLE);
                        }
                    });
                }

                FilterResults filterResults = new FilterResults();

                if (constraint != null) {
                    PhoneContacts pc;
                    if (Pattern.matches("[a-zA-Z]+", constraint.toString()))
                        pc = allPhoneContacts.filterContactsByName(constraint.toString());
                    else
                        pc = allPhoneContacts.filterContactsByPhoneNumber(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = pc;
                    filterResults.count = pc.getNumberOfContacts();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (mLoadingIndicator != null) {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        public void run() {
                            mLoadingIndicator.setVisibility(View.GONE);
                        }
                    });
                }
                updatePhoneContacts((PhoneContacts)results.values);
            }};
        return filter;
    }

    public void updatePhoneContacts(PhoneContacts newPhoneContacts){
        if (newPhoneContacts != null) {
            contacts = newPhoneContacts;
        } else {
            contacts = allPhoneContacts;
        }
        this.notifyDataSetChanged();
    }

    public void setLoadingIndicator(ProgressBar progressBar) {
        mLoadingIndicator = progressBar;
    }
}

