package com.example.laviniabratis.androidautocomplete.helper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.laviniabratis.androidautocomplete.activity.AutoCompleteFragment;
import com.example.laviniabratis.androidautocomplete.activity.AutoCompleteWithListFragment;
import com.example.laviniabratis.androidautocomplete.activity.FilteringFragment;

/**
 * Created by lavinia.bratis on 9/8/15.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mNoOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNoOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                AutoCompleteFragment tab1 = new AutoCompleteFragment();
                return tab1;
            case 1:
                AutoCompleteWithListFragment tab2 = new AutoCompleteWithListFragment();
                return tab2;
            case 2:
                FilteringFragment tab3 = new FilteringFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}
