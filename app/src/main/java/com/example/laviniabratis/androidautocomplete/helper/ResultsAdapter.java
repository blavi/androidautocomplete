package com.example.laviniabratis.androidautocomplete.helper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.laviniabratis.androidautocomplete.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavinia.bratis on 9/3/15.
 */
public class ResultsAdapter extends BaseAdapter implements Filterable{

    private Context mContext;
    private List<String> resultList = new ArrayList<String>();
    private ProgressBar mLoadingIndicator;

    public ResultsAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.result_item, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.result)).setText(getItem(position));
        return convertView;
    }

    public void setLoadingIndicator(ProgressBar progressBar) {
        mLoadingIndicator = progressBar;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (mLoadingIndicator != null) {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        public void run() {
                            mLoadingIndicator.setVisibility(View.VISIBLE);
                        }
                    });
                }

                FilterResults filterResults = new FilterResults();
                List<String> results;
                if (constraint != null) {

                    results = GetResults.getInstance(mContext).loadFromWeb(constraint.toString());

                    // Assign the data to the FilterResults
                    filterResults.values = results;
                    filterResults.count = results.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (mLoadingIndicator != null) {
                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        public void run() {
                            mLoadingIndicator.setVisibility(View.GONE);

                        }
                    });
                }

                if (results != null && results.count > 0) {
                    resultList = (List<String>) results.values;
                } else {
                    resultList.clear();
                }
                notifyDataSetChanged();
            }};
        return filter;
    }
}
