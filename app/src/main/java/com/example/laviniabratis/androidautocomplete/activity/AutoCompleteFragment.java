package com.example.laviniabratis.androidautocomplete.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.laviniabratis.androidautocomplete.R;
import com.example.laviniabratis.androidautocomplete.helper.DelayAutoCompleteTextView;
import com.example.laviniabratis.androidautocomplete.helper.ResultsAdapter;

/**
 * Created by lavinia.bratis on 9/3/15.
 */
public class AutoCompleteFragment extends Fragment {
    private DelayAutoCompleteTextView searchInput;
    private ResultsAdapter mAdapter;
    private Activity activity;

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);

        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.first_tab, container, false);

        setLayout(view);

        return view;
    }

    private void setLayout(View view){
        searchInput = (DelayAutoCompleteTextView) view.findViewById(R.id.searchInput);
        searchInput.setLoadingIndicator((android.widget.ProgressBar) view.findViewById(R.id.pbLoadingIndicator));

        mAdapter = new ResultsAdapter(activity);
        searchInput.setAdapter(mAdapter);
        searchInput.setThreshold(1);
    }
}
