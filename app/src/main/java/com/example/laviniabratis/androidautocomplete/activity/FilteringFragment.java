package com.example.laviniabratis.androidautocomplete.activity;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.app.ProgressDialog;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.laviniabratis.androidautocomplete.R;
import com.example.laviniabratis.androidautocomplete.helper.ContactsCursorAdapter;
import com.example.laviniabratis.androidautocomplete.model.Contact;
import com.example.laviniabratis.androidautocomplete.model.PhoneContacts;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lavinia.bratis on 9/8/15.
 */
public class FilteringFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {


    private EditText searchInput;
    private ListView contactsList;
    private ContactsCursorAdapter mCursorAdapter;
    private ProgressBar pbLoadingIndicator;
    private View view;

    private static final String[] PROJECTION =
            {
                    ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER
            };

    private Activity activity;
    private TextWatcher searchTextWatcher;
    private Timer timer;
    private int AUTOCOMPLETE_DELAY = 500;

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);

        activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view =  inflater.inflate(R.layout.third_tab, container, false);

        setLayout();

        return view;
    }

    private void setLayout(){
        searchInput = (EditText) view.findViewById(R.id.searchInput);
        contactsList = (ListView) view.findViewById(R.id.contactsList);
        pbLoadingIndicator = (ProgressBar) view.findViewById(R.id.pbLoadingIndicator);

        setContactsList();
    }

    private void setSearchContactWatcher(){
        final Filter filter = mCursorAdapter.getFilter();
        searchTextWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // user typed: start the timer
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        filter.filter(searchInput.getText().toString());
                        // hide keyboard
                        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }, AUTOCOMPLETE_DELAY); // 500ms delay before the timer executes the „run“ method from TimerTask
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
            }
        };

        searchInput.addTextChangedListener(searchTextWatcher);
    }

    private void setContactsList(){

        // Initializes the loader
        getLoaderManager().initLoader(0, null, FilteringFragment.this);
    }


    @Override
    public CursorLoader onCreateLoader(int id, Bundle args) {
        pbLoadingIndicator.setVisibility(View.VISIBLE);
        Uri baseUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(
                activity,
                baseUri,
                PROJECTION,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data){
        pbLoadingIndicator.setVisibility(View.GONE);

        // set adapter
        mCursorAdapter = new ContactsCursorAdapter(activity, new PhoneContacts(extractPhoneContacts(data)));
        mCursorAdapter.setLoadingIndicator(pbLoadingIndicator);
        contactsList.setAdapter(mCursorAdapter);
        setSearchContactWatcher();
    }

    private List<Contact> extractPhoneContacts(Cursor data){
        ArrayList<Contact> contacts = new ArrayList<>();

        while (data.moveToNext()){
            contacts.add(fromCursor(data));
        }

        return contacts;
    }

    public Contact fromCursor(Cursor data) {
        String name = data.getString(data.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
        String phoneNumber = data.getString(data.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

        return new Contact(name, phoneNumber);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.updatePhoneContacts(null);
    }
}
